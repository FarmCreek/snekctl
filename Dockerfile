FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
RUN mkdir devices
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS publish
WORKDIR /src
COPY src/SnekCtlWebApi/SnekCtlWebApi.csproj .
RUN dotnet restore SnekCtlWebApi.csproj
COPY src/SnekCtlWebApi/ . 
RUN dotnet publish SnekCtlWebApi.csproj -c release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet" , "SnekCtlWebApi.dll"]