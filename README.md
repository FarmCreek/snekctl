# SnekCtl
This project serves two purposes. First, I want to get an introduction to implement a microservice. This will be my very first. Secondly, it is intended to solve a real world problem in form of controlling the lighting and temperature for our hognose snakes. 

For this project i expect to learn how to:
    
    1. Get further familarity with the GitLab CI/CD cycle
    2. Create and manage a Docker container to containerise the microservice
    3. Write a microservice with .NET 5.0 and have it serve an REST api for my internal network. 

I expect this project to serve to a frontend, which will be in another repository on my [GitLab page](https://gitlab.com/FarmCreek/). A link to this project can be found [here](https://gitlab.com/FarmCreek/snekctlwebapp)